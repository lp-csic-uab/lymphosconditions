#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
Creates a Json Database ready to be used by the Lymphos framework
from an excel csv file filled by the user

Names of input and output files are read from a configuration file with the
following content:

#conditions.cfg

[Archives]
csvfile=experiments_worksheet_latest.csv
jsonfile=experiments_latest.db
"""

__version__ = '0.1.1'
__author__ = "Joaquin Abian"

import csv
import json
import ConfigParser


CONFIG_FILENAME = 'conditions.cfg'

def write_csv_2_json(archive, destination):
    ""
    new_dictio = {}
    with open(archive, 'rU') as mysource:
        with open(destination, 'w') as mydestination:
            reader = csv.DictReader(mysource)
            for dictio in reader:
                key = dictio[u'experiment_id']
                new_dictio[key] = dictio
                
            json.dump(new_dictio, mydestination, indent=4)
    
def main():
    
    config = ConfigParser.ConfigParser()
    config.read(CONFIG_FILENAME)
    
    try:
        write_csv_2_json(config.get("Archives", "csvfile"),
                         config.get("Archives", "jsonfile"))
    except IOError, e:
        print str(e)
    

if __name__ == '__main__':
    
    main()
