---

**WARNING!**: This is the *Old* source-code repository for the Conditions script needed for the conditions data conversion for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/conditions_code/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/conditions_code/*  

---  
  
# LymPHOS2 Conditions

Script to convert conditions tables to JSON format, for importing them in LymPHOS2 DB.
  
  
---

**WARNING!**: This is the *Old* source-code repository for the Conditions script needed for the conditions data conversion for the web-app at [www.LymPHOS.org](https://www.LymPHOS.org).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/lymphosweb/conditions_code/) located at https://sourceforge.net/p/lp-csic-uab/lymphosweb/conditions_code/*  

---  
